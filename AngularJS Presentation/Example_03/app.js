﻿angular.module('myApp', [])
    .controller('myController', function($scope) {
        $scope.title = "Incrementor";
        $scope.value = 1;
        $scope.incrementor = function() {
            $scope.value++;
        }
    });
